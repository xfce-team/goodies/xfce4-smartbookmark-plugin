# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-03 21:17+0200\n"
"PO-Revision-Date: 2018-04-25 12:56+0000\n"
"Last-Translator: Zmicer Turok <nashtlumach@gmail.com>\n"
"Language-Team: Belarusian (http://www.transifex.com/xfce/xfce-panel-plugins/language/be/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: be\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: ../src/smartbookmark.c:90
#, c-format
msgid "Failed to send %s to your preferred browser"
msgstr "Не атрымалася адправіць %s вашаму пераважнаму браўзеру"

#: ../src/smartbookmark.c:259
msgid "Smartbookmark"
msgstr "Разумныя закладкі"

#: ../src/smartbookmark.c:264
msgid "Preferences"
msgstr "Налады"

#. text label
#: ../src/smartbookmark.c:279
msgid "Label:"
msgstr "Адмеціна:"

#. size label
#: ../src/smartbookmark.c:294
msgid "Size:"
msgstr "Памер:"

#. Hide label option
#: ../src/smartbookmark.c:305
msgid "Hide label"
msgstr "Схаваць адмеціну"

#. url label
#: ../src/smartbookmark.c:316
msgid "URL:  "
msgstr "URL: "

#: ../src/smartbookmark.desktop.in.h:1
msgid "SmartBookmark"
msgstr "Разумныя закладкі"

#: ../src/smartbookmark.desktop.in.h:2
msgid "Query websites from the Xfce panel"
msgstr "Пераход да сайтаў з панэлі Xfce"
